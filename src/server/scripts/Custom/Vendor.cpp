#include "ScriptPCH.h"

enum Action
{
    MENU,
};

class vip_vendors : public CreatureScript
{
    public:
        vip_vendors() : CreatureScript("vip_vendors") { }

        bool OnGossipHello(Player * player, Creature * creature)
        {
            player->ADD_GOSSIP_ITEM(9, "[Menu] ->", GOSSIP_SENDER_MAIN, MENU);
            player->PlayerTalkClass->SendGossipMenu(907, creature->GetGUID());
            return true;
        }
				
        bool OnGossipSelect(Player * player, Creature * creature, uint32 /*uiSender*/, uint32 Action)
	{
            switch(Action)
            {
                case MENU:
                {
                    if(player->GetSession()->GetSecurity() == 0) //if is a player
                        player->GetSession()->SendNotification("Vip Vendors are locked, to unlock them visit http://www.darknesswow.ml/donate");
                    else
                        player->GetSession()->SendListInventory(creature->GetGUID());
                }
                break;
            }
            return true;
        }
};   
void AddSC_vip_vendors()
{        
    	new vip_vendors();
}		
