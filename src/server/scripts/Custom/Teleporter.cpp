#include "ScriptPCH.h"

enum Action 
{
     MALL,
     DALARAN,
     STORMWIND,
     ORGRIMMAR,
     DUELZONE,
     SHATTRATH,
     VIPMALL,
     VIPQUEUE,
     VIPISLAND,
     QUEUE,
     TRANSMOG,
     EVENT,
     DUMMY,
};

class npc_teleporter : public CreatureScript
{
    public:
        npc_teleporter() : CreatureScript("npc_teleporter") { }
				
        bool OnGossipHello(Player *player, Creature *creature)
        { 
            player->ADD_GOSSIP_ITEM(2, "Mall", GOSSIP_SENDER_MAIN, MALL);
            if (player->GetTeam() == ALLIANCE)
            player->ADD_GOSSIP_ITEM(2, "Stormwind", GOSSIP_SENDER_MAIN, STORMWIND);
            else
            player->ADD_GOSSIP_ITEM(2, "Orgrimmar", GOSSIP_SENDER_MAIN, ORGRIMMAR);
            player->ADD_GOSSIP_ITEM(2, "Dalaran", GOSSIP_SENDER_MAIN, DALARAN);
            player->ADD_GOSSIP_ITEM(2, "Shattrath", GOSSIP_SENDER_MAIN, SHATTRATH);
            player->ADD_GOSSIP_ITEM(2, "Duel Zone", GOSSIP_SENDER_MAIN, DUELZONE);
            if (player->GetSession()->GetSecurity() == 0)
            player->ADD_GOSSIP_ITEM(2, "Random Queuing Places", GOSSIP_SENDER_MAIN, QUEUE);
            player->ADD_GOSSIP_ITEM(2, "Transmog Zone", GOSSIP_SENDER_MAIN, TRANSMOG);
            player->ADD_GOSSIP_ITEM(2, "Stairs Event Zone", GOSSIP_SENDER_MAIN, EVENT);
            player->ADD_GOSSIP_ITEM(2, "Training Dummy Area", GOSSIP_SENDER_MAIN, DUMMY);
            player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());

            if (player->GetSession()->GetSecurity() != 0)
            {
                player->ADD_GOSSIP_ITEM(2, "VIP Mall", GOSSIP_SENDER_MAIN, VIPMALL);
                player->ADD_GOSSIP_ITEM(2, "VIP Random Queuing Places", GOSSIP_SENDER_MAIN, VIPQUEUE);
                player->ADD_GOSSIP_ITEM(2, "VIP Island", GOSSIP_SENDER_MAIN, VIPISLAND);
                player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
            }
	    return true;
        }
				
        bool OnGossipSelect(Player * player, Creature * creature, uint32 /*uiSender*/, uint32 Action)
	{
            switch(Action)
            {
                case MALL:
                {
	            player->TeleportTo(530, 281.260651, 6035.553223, 130.266754, 1.570533); //MALL
	            player->PlayerTalkClass->SendCloseGossip();
                }
                break;

                case DALARAN:
                {
		    if (player->GetTeam() == ALLIANCE)
                    {
                        player->TeleportTo(571, 5760.059570, 714.006836, 641.677368, 5.620006); //DALARAN
		        player->PlayerTalkClass->SendCloseGossip();
                    }
                    else
                    {
                        player->TeleportTo(571, 5865.734375, 589.876038, 650.124451, 2.471576); //DALARAN
		        player->PlayerTalkClass->SendCloseGossip();
                    }
                }
                break;

                case STORMWIND:
                {
		    player->TeleportTo(0, -8826.893555, 628.756409, 94.050201, 3.891258); //STORMWIND
		    player->PlayerTalkClass->SendCloseGossip();
                }
	        break;

                case ORGRIMMAR:
                {
		    player->TeleportTo(1, 1629.938599, -4373.593262, 31.535618, 3.535618); //ORGRIMMAR
		    player->PlayerTalkClass->SendCloseGossip();
                }
	        break;

                case DUELZONE:
                {
		    player->TeleportTo(1, 190.810593, 1181.776855, 168.298431, 3.065889); //DUEL ZONE        
		    player->PlayerTalkClass->SendCloseGossip();
                }
                break;

                case SHATTRATH:
                {
                    if (player->GetTeam() == ALLIANCE)
                    {
                        player->TeleportTo(530, -1801.791626, 5409.462402, -12.427621, 2.739202); //SHATTRATH
		        player->PlayerTalkClass->SendCloseGossip();
                    }       
                    else
                    {
                        player->TeleportTo(530, -1925.840820, 5450.417480, -12.428013, 6.005977); //SHATTRATH
		        player->PlayerTalkClass->SendCloseGossip();
                    }
                }
                break;

                case VIPMALL:
                {
                    //player->TeleportTo(530, -1925.840820, 5450.417480, -12.428013, 6.005977); //VIP MALL
                    player->GetSession()->SendNotification("This function is currently disabled, please check the changelog for more informations.");
		    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;

                case VIPQUEUE:
                {
                    int Nrandom;
                    Nrandom = urand(1, 6);

                    if (Nrandom == 1)
                    {
                        player->TeleportTo(530, -863.868530, 6975.276855, 34.885334, 6.020584); //Nagrand
                        player->PlayerTalkClass->SendCloseGossip();
                    }
                    else if (Nrandom == 2)
                    {
                        player->TeleportTo(530, -1724.619263, 7445.014648, 211.61822, 4.530030); //Nagrand
                        player->PlayerTalkClass->SendCloseGossip();
                    }
                    else if (Nrandom == 3)
                    { 
                        player->TeleportTo(530, -879.681458, 8692.606445, 251.571365, 4.860143); //Warmail Hill
                        player->PlayerTalkClass->SendCloseGossip();
                    }
                    else if (Nrandom == 4)
                    {
                        player->TeleportTo(1, -8843.157227, -4784.713867, 0.981628, 0.906070); //Tanaris
                        player->PlayerTalkClass->SendCloseGossip();
                    }
                    else if (Nrandom == 5)
                    {
                        player->TeleportTo(530, -2310.471191, 3079.747070, 158.385315, 0.610325); //Firewing Point
                        player->PlayerTalkClass->SendCloseGossip();
                    }
                    else if (Nrandom == 6) 
                    {
                        player->TeleportTo(530, -398.983185, 3138.785400, 4.719993, 6.045997); //Hellfire Basin
                        player->PlayerTalkClass->SendCloseGossip();
                    }
                }
                break;

                case QUEUE:
                {
                    int Nrandom;
                    Nrandom = urand(1, 3);

                    if (Nrandom == 1)
                    {
                        player->TeleportTo(530, -863.868530, 6975.276855, 34.885334, 6.020584); //Nagrand
                        player->PlayerTalkClass->SendCloseGossip();
                    }
                    else if (Nrandom == 2)
                    {  
                        player->TeleportTo(530, -1724.619263, 7445.014648, 211.61822, 4.530030); //Nagrand
                        player->PlayerTalkClass->SendCloseGossip();
                    }
                    else if (Nrandom == 3)
                    {
                        player->TeleportTo(530, -879.681458, 8692.606445, 251.571365, 4.860143); //Warmail Hill
                        player->PlayerTalkClass->SendCloseGossip();
                    }
                }
                break;

                case VIPISLAND:
                {
                    player->TeleportTo(1, -11851.464844, -4816.557617, 10.625483, 1.110058); //VIP ISLAND
                    player->PlayerTalkClass->SendCloseGossip();		
                }
                break;

                case TRANSMOG:
                {
                    player->TeleportTo(530, 2408.511963, 2761.904053, 135.384003, 4.810338); //Transmog Zone
                    player->PlayerTalkClass->SendCloseGossip();
                }
                break;

                case EVENT:
                {
                    //player->TeleportTo(571, 5820.903809, 423.841461, 658.783875, 5.012739); //Stairs Event Zone
                    player->GetSession()->SendNotification("This function is currently disabled, please check the changelog for more informations.");
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                    //player->PlayerTalkClass->SendCloseGossip();
                }
                break;
 
                case DUMMY:
                {
                    player->TeleportTo(530, 912.806458, 2270.181641, 310.259094, 6.250832);
                    player->PlayerTalkClass->SendCloseGossip();
                }
                break;
            }
            return true;
        }
};				

void AddSC_npc_teleporter()
{        
	new npc_teleporter();
}		
