#include "ScriptPCH.h"

enum Actions
{
    CHEF,
    CHRISTMAS,
    MEXICAN,
    PIRATE,
    FARMER, 
    BUNNY,
    REMOVE
};

class npc_transmog : public CreatureScript
{
    public: 
        npc_transmog() : CreatureScript("npc_transmog") { }		

     	bool OnGossipHello(Player * player, Creature * creature)
	{
	    if (player->GetSession()->GetSecurity() == 0) //if player isn't V.I.P
            {
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Donator - Chef (locked)", GOSSIP_SENDER_MAIN, 100);
      	        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Donator - Christmas (locked)", GOSSIP_SENDER_MAIN, 100);
       	        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Donator - Mexican (locked)", GOSSIP_SENDER_MAIN, 100);
	        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Donator - Pirate (locked)", GOSSIP_SENDER_MAIN, 100);
	        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Donator - Farmer (locked)", GOSSIP_SENDER_MAIN, 100); 
	        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Donator - Bunny (locked)", GOSSIP_SENDER_MAIN, 100);
	        player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
            } 
            else // if player is V.I.P
	    { 
               player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Donator - Chef", GOSSIP_SENDER_MAIN, CHEF);
   	       player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Donator - Christmas", GOSSIP_SENDER_MAIN, CHRISTMAS);
	       player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Donator - Mexican", GOSSIP_SENDER_MAIN, MEXICAN);
               player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Donator - Pirate", GOSSIP_SENDER_MAIN, PIRATE);
               player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Donator - Farmer", GOSSIP_SENDER_MAIN, FARMER);
	       player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Donator - Bunny", GOSSIP_SENDER_MAIN, BUNNY);
	       player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "[Unavailable] Remove Transmogrifications", GOSSIP_SENDER_MAIN, REMOVE);
               player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
            }
            return true;
	}

	void UpdateGear(Player * player)
        {
	    player->SendUpdateToPlayer(player); // First Send update to player, so most recent datas are up
	    WorldPacket data(SMSG_FORCE_DISPLAY_UPDATE, 8); // Force client to reload this player, so changes are visible
	    data << player->GetGUID();
	    player->SendMessageToSet(&data,true);
	    player->CastSpell(player, 24222, true); // Do some visual effect ( Vanish visual spell )
        }

        bool OnGossipSelect(Player * player, Creature * /*creature*/, uint32 /*sender*/, uint32 Action)
        {
            switch(Action)
            {             
	        case CHEF:
	        {
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_1_ENTRYID,  46349); // helm
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_3_ENTRYID,      0); // shoulder
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_4_ENTRYID,      0); // shirt
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_5_ENTRYID,  10040); // chest
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_6_ENTRYID,      0); // waist
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_7_ENTRYID,      0); // pants
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_8_ENTRYID,  19438); // boots
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_10_ENTRYID,     0); // gloves
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_15_ENTRYID,     0); // back
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_16_ENTRYID, 43611); // weapon
                    ChatHandler(player->GetSession()).PSendSysMessage("|cff00ccffNice, %s now you looks like a real Chef!", player->GetName());
                    UpdateGear(player);
	            player->PlayerTalkClass->SendCloseGossip();
                }
                break;

	        case CHRISTMAS:
	        {  
                    int Nrandom;
                    Nrandom = urand(1, 2);

                    if (Nrandom == 1) //GREEN CLOTHS
                    { 
	                player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_1_ENTRYID,  21525); // helm
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_3_ENTRYID,      0); // shoulder
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_4_ENTRYID,      0); // shirt
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_5_ENTRYID,  34087); // chest
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_6_ENTRYID,      0); // waist
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_7_ENTRYID,      0); // pants
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_8_ENTRYID,  34086); // boots
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_10_ENTRYID,     0); // gloves
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_15_ENTRYID,     0); // back
	                player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_16_ENTRYID,     0); // weapon
                        ChatHandler(player->GetSession()).PSendSysMessage("|cff00ccffNice, %s you are ready for a Christmas party!", player->GetName());
	                UpdateGear(player);
	                player->PlayerTalkClass->SendCloseGossip();
                   }
                   else if (Nrandom == 2) //RED CLOTHS
                   {
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_1_ENTRYID,  21524); // helm
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_3_ENTRYID,      0); // shoulder
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_4_ENTRYID,      0); // shirt
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_5_ENTRYID,  34085); // chest
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_6_ENTRYID,      0); // waist
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_7_ENTRYID,      0); // pants
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_8_ENTRYID,  34086); // boots
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_10_ENTRYID,     0); // gloves
                        player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_15_ENTRYID,     0); // back
	                player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_16_ENTRYID,     0); // weapon
                        ChatHandler(player->GetSession()).PSendSysMessage("|cff00ccffNice, %s you are ready for a Christmas party!", player->GetName());
                        UpdateGear(player);
	                player->PlayerTalkClass->SendCloseGossip();
                   }
 	        }
	        break;

	        case MEXICAN:
	        {
	            player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_1_ENTRYID,  38506); // helm
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_3_ENTRYID,      0); // shoulder
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_4_ENTRYID,      0); // shirt
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_5_ENTRYID,  10036); // chest
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_6_ENTRYID,      0); // waist
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_7_ENTRYID,  10035); // pants
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_8_ENTRYID,      0); // boots
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_10_ENTRYID,     0); // gloves
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_15_ENTRYID,     0); // back
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_16_ENTRYID,     0); // weapon
                    ChatHandler(player->GetSession()).PSendSysMessage("|cff00ccffNice, %s you looks like a mexican!", player->GetName());
	            UpdateGear(player);
	            player->PlayerTalkClass->SendCloseGossip();
	        }
                break;

	        case PIRATE:
	        {		
	            player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_1_ENTRYID,    2955); // helm
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_3_ENTRYID,       0); // shoulder
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_4_ENTRYID,       0); // shirt
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_5_ENTRYID,   22742); // chest
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_6_ENTRYID,   22743); // waist
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_7_ENTRYID,    4044); // pants
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_8_ENTRYID,   22744); // boots
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_10_ENTRYID,      0); // gloves
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_15_ENTRYID,      0); // back
	            player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_16_ENTRYID,  31336); // weapon
                    ChatHandler(player->GetSession()).PSendSysMessage("|cff00ccffNice, %s you looks like a real Pirate!, arrg", player->GetName());
	            UpdateGear(player);
	            player->PlayerTalkClass->SendCloseGossip();
                }
	        break;				

	        case FARMER:
	        {
	            player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_1_ENTRYID,   24745); // helm
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_3_ENTRYID,       0); // shoulder
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_4_ENTRYID,       0); // shirt
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_5_ENTRYID,   24743); // chest
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_6_ENTRYID,       0); // waist
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_7_ENTRYID,   24746); // pants
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_8_ENTRYID,   11191); // boots
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_10_ENTRYID,   7480); // gloves
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_15_ENTRYID,      0); // back 
	            player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_16_ENTRYID,  19963); // weapon
                    ChatHandler(player->GetSession()).PSendSysMessage("|cff00ccffNice, %s you looks like a real Farmer!", player->GetName());
	            UpdateGear(player);
	            player->PlayerTalkClass->SendCloseGossip();
	        } 
       	        break;
					
                case BUNNY:
	        {
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_1_ENTRYID,   44803); // helm
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_3_ENTRYID,       0); // shoulder
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_4_ENTRYID,       0); // shirt
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_5_ENTRYID,   20034); // chest
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_6_ENTRYID,       0); // waist
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_7_ENTRYID,       0); // pants
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_8_ENTRYID,   19438); // boots
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_10_ENTRYID,      0); // gloves 
                    player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_15_ENTRYID,      0); // back
	            player->UpdateUInt32Value(PLAYER_VISIBLE_ITEM_16_ENTRYID,  45073); // weapon
                    ChatHandler(player->GetSession()).PSendSysMessage("|cff00ccffNice, %s you looks like a real Bunny, very sexy!", player->GetName());
	            UpdateGear(player);
                    player->PlayerTalkClass->SendCloseGossip();
                }
    	        break;
	    
                case REMOVE: //REMOVE ALL TRANSMOGRIFICATION
	        {
                    player->GetSession()->SendNotification("This function is currently disabled, please check the changelog for more informations.");
                   //ChatHandler(player->GetSession()).PSendSysMessage("Your Set Has Been Removed!");
                   //UpdateGear(player);
	           player->PlayerTalkClass->SendCloseGossip();
	        }
	        break;

	        case 100:
	        { 
	            player->GetSession()->SendNotification("The Donation is locked, to unlock it visit darknesswow.com/donate");
	            player->PlayerTalkClass->SendCloseGossip();
	        } 
	        break;
            }
            return true;	
    }	
};

void AddSC_npc_transmog()
{
 new npc_transmog();
}
