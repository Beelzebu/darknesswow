#include "ScriptPCH.h"

class npc_helper : public CreatureScript
{ 
    public:
        npc_helper() : CreatureScript("npc_helper") { }

        bool OnGossipHello(Player * player, Creature * creature)
        {
            player->ADD_GOSSIP_ITEM(7, "How can i open a ticket?", GOSSIP_SENDER_MAIN, 1);
            player->ADD_GOSSIP_ITEM(7, "How can i report a bug?", GOSSIP_SENDER_MAIN, 2);
	    player->ADD_GOSSIP_ITEM(7, "How can i report a player?", GOSSIP_SENDER_MAIN, 3);
	    player->ADD_GOSSIP_ITEM(7, "How can i vote for the server?", GOSSIP_SENDER_MAIN, 4);
            player->ADD_GOSSIP_ITEM(7, "How can i join on DarknessWoW Staff?", GOSSIP_SENDER_MAIN, 5); 
            player->ADD_GOSSIP_ITEM(7, "How can i join on global chat?", GOSSIP_SENDER_MAIN, 6);
	    player->ADD_GOSSIP_ITEM(7, "How can i go to duel zone?", GOSSIP_SENDER_MAIN, 7);
            player->ADD_GOSSIP_ITEM(7, "How can i get golds?", GOSSIP_SENDER_MAIN, 8);
            player->ADD_GOSSIP_ITEM(7, "How can i join battlegrounds?", GOSSIP_SENDER_MAIN, 9);
	    player->ADD_GOSSIP_ITEM(7, "How can i join arena?", GOSSIP_SENDER_MAIN, 10);
            player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
    	    return true;
        }			
	 
        bool OnGossipSelect(Player * player, Creature * creature, uint32 /*uiSender*/, uint32 Action)
        { 
            switch(Action)
            {
                case 1:
                {
	            ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[How can i open a ticket?]:|r Hello Dear Player, if you want open a ticket go to Game Menu it has a red question mark click it, then you have to click ''Talk to a GM'', a GM will assist you soon as possible.");
	            player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;
           
                case 2:
                {
	            ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[How can i report a bug?]:|r Hello Dear Player, if you want to report a bug you need to go to bug report section on forum. Make sure to follow the right format, an Administrator will assist you soon as possible.");	        
	            player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;
					
                case 3:
                {
	            ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[How can i report a player?]:|r Hello Dear Player, if you want report a player for any reason you need to go on Player/GM Report on the forum and make a new topic, make sure to follow the right format.");
	            player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
	        break;

                case 4:
                {
	            ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[How can i vote for the server?]:|r Hello Dear Player, if you want vote the server you need to go http://www.Omega-wow.com/vote each vote  will give you 1 vote point, with vote point you can buy mounts, pets, armors, weapons on http://www.omega-wow.com/shop.");
	            player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;

                case 5:
                {
	            ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[How can i join on DarknessWoW Staff?]:|r Hello Dear Player, if you want join DarknessWoW Staff you need to go on forum and then go to ''Apply for GameMaster'', please follow the format and make sure that you meet the requirements.");
	            player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;
                    
                case 6:
                {
	            ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[How can i use the World Chat?]:|r Hello Dear Player, if you want use the World Chat you need to type .chat or .c");
	            player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;
                    
                case 7:
                {
	            ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[How can i go to duel zone?]:|r Hello Dear Player, if you want go on the Duel Zone you need to use our Teleporter Npc and choose Duel Zone.");
	            player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;

                case 8:
                {
	            ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[How can i get golds?]:|r Hello Dear Player, if you want to get gold you can just use the vendor we added (Gold System)  and recieve each time you are using it 5.000 golds. It's the faster and easiest way!.");
	            player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;
           
                case 9:
                {
                    ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[How can i join battlegrounds?]:|r Hello Dear Player, if you wish to join in a batleground you need to press button H and make sure that you are in Honor window. Then simply select a batleground to play. Please be patient in the queue.");
	            player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;				
            
                case 10:
                {
	            ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[How can i join arena?]:|r Hello Dear Player, if you want to queue for arena rated matches you need to press button H and make sure that you are in Arena window and create an arena team. Then invite your partener(s) to a party group and then click on Conquest and join batle!");     
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;
           }
           return true;
         }
};				

void AddSC_npc_helper()
{        
	new npc_helper();
}
