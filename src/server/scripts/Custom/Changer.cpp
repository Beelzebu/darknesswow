#include "ScriptPCH.h"

enum Actions 
{
  RACE,
  FACTION,
  NAME,
  CANCEL,
  PLAYER,
};

class npc_changer : public CreatureScript
{
    public:
        npc_changer() : CreatureScript("npc_changer") { }

        bool OnGossipHello(Player * player, Creature * creature)
        {				
            player->ADD_GOSSIP_ITEM_EXTENDED(4, "Change my race ", GOSSIP_SENDER_MAIN, RACE, "Are you sure to change your race?", 0, false);
            player->ADD_GOSSIP_ITEM_EXTENDED(4, "Change my faction", GOSSIP_SENDER_MAIN, FACTION, "Are you sure to change your faction?", 0, false);
            player->ADD_GOSSIP_ITEM_EXTENDED(4, "Change my name", GOSSIP_SENDER_MAIN, NAME, "Are you sure to change your name?", 0, false);
            player->ADD_GOSSIP_ITEM_EXTENDED(4, "Cancel actions", GOSSIP_SENDER_MAIN, CANCEL, "Are you sure to cacel your actions?", 0, false);
            player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());			        
            return true;
	} 			
          
        bool OnGossipSelect(Player * player, Creature * creature, uint32 /*Sender*/, uint32 Action)
        { 
            switch(Action)
            {
                case RACE: //RACE
                {   
                    if(player->GetSession()->GetSecurity() == 0)
                    {
                      if(player->HasItemCount(20880, 175))
                      {
                          player->DestroyItemCount(20880, 175, true);
                          player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
                          CharacterDatabase.PExecute("UPDATE characters SET at_login = at_login | '128' WHERE guid = %u", player->GetGUID());
                          player->GetSession()->SendNotification("You need to relog, to change your race!");
                          player->PlayerTalkClass->SendCloseGossip();
                      }
                      else
                      {
                          player->GetSession()->SendNotification("To change your race you need at least 175 Golden Tokens");
                          player->PlayerTalkClass->SendCloseGossip();
                      }
                    }
                    else
                    {
                        player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
                        CharacterDatabase.PExecute("UPDATE characters SET at_login = at_login | '128' WHERE guid = %u", player->GetGUID());
                        player->GetSession()->SendNotification("You need to relog, to change your race!");
                        player->PlayerTalkClass->SendCloseGossip();
                   }
                }
                break;
            
                case FACTION: //FACTION
                {
                    if(player->GetSession()->GetSecurity() == 0)
                    {
                      if(player->GetItemCount(20880, 275))
                      {
                          player->DestroyItemCount(20880, 275, true);
                          player->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
                          CharacterDatabase.PExecute("UPDATE characters SET at_login = at_login | '64' WHERE guid = %u", player->GetGUID());
                          player->GetSession()->SendNotification("You need to relog to change your faction!");
                          player->PlayerTalkClass->SendCloseGossip();
                      }
                      else
                      {
                          player->GetSession()->SendNotification("To change your faction you need at least 275 Golden Tokens");
                          player->PlayerTalkClass->SendCloseGossip();
                      }
                    }
                    else
                    {
                        player->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
                        CharacterDatabase.PExecute("UPDATE characters SET at_login = at_login | '64' WHERE guid = %u", player->GetGUID());
                        player->GetSession()->SendNotification("You need to relog to change your faction!");
                        player->PlayerTalkClass->SendCloseGossip();
                    }
                }
                break;
            
                case NAME: //NAME
                {
                    if(player->GetSession()->GetSecurity() == 0)
                    {
                      if(player->GetItemCount(20880, 75))
                      {
                          player->DestroyItemCount(20880, 75, true);
                          player->SetAtLoginFlag(AT_LOGIN_RENAME);
                          player->GetSession()->SendNotification("You need to relog to change your name!");
                          player->PlayerTalkClass->SendCloseGossip();
                      }
                      else
                      {
                          player->GetSession()->SendNotification("To change your name you need at least 75 Golden Tokens");
                          player->PlayerTalkClass->SendCloseGossip();
                      }
                    }
                    else
                    {
                        player->SetAtLoginFlag(AT_LOGIN_RENAME);
                        player->GetSession()->SendNotification("You need to relog to change your name!");
                        player->PlayerTalkClass->SendCloseGossip();
                    }
                }
                break;

                case CANCEL: //CANCEL
                {
                    player->RemoveAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
                    player->RemoveAtLoginFlag(AT_LOGIN_CHANGE_RACE);
                    player->RemoveAtLoginFlag(AT_LOGIN_RENAME);
                    player->GetSession()->SendNotification("Your actions has been cancelled!");
                    player->PlayerTalkClass->SendCloseGossip();
                }
                break;
             }
	     return true;
         }		
};

void AddSC_npc_changer()
{
  new npc_changer();
}
