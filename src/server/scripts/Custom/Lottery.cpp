#include "ScriptPCH.h" 

enum Action 
{
 WINNER,
 BUY,
 END, 
};

class npc_lottery : public CreatureScript
{
    public:
        npc_lottery() : CreatureScript("npc_lottery") { }
				
        bool OnGossipHello(Player * player, Creature * creature)
        {
            if(player->GetSession()->GetSecurity() == 0 or player->GetSession()->GetSecurity() == 1)
           {
               player->ADD_GOSSIP_ITEM(10, "Show me the latest winner!", GOSSIP_SENDER_MAIN, WINNER);
               player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "Buy a ticket for 2 golds!", GOSSIP_SENDER_MAIN, BUY, "Are you sure to buy the ticket?", 0, false);
               player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
           }
           else
           {
               player->ADD_GOSSIP_ITEM(10, "Show me the latest winner!", GOSSIP_SENDER_MAIN, WINNER);
               player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "Buy a ticket", GOSSIP_SENDER_MAIN, BUY, "Are you sure to buy the ticket?", 0, false);
               player->ADD_GOSSIP_ITEM(0, "<Debug> END", GOSSIP_SENDER_MAIN, END);
               player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
           }
           return true;
        }
 			
        bool OnGossipSelect(Player * player, Creature * creature, uint32 /*uiSender*/, uint32 Action)
        {
            switch(Action)
            {
                case WINNER: //show latest winner
                {
                    QueryResult result = WorldDatabase.PQuery("SELECT name FROM lottery_winner");
                
                    if(result)
                    {
                        Field* field = result->Fetch();
                        ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Show me the latest winner!]:|r Dear Player, the latest lottery winner is: %s.", field[0].GetCString());
                        player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                    } 
                }
                break;
             
                case BUY:
                {
                    if(player->HasEnoughMoney(20000)) //if player have money
                    {//check if the player is in the lottery table
                        if(WorldDatabase.PQuery("SELECT guid FROM lottery_players WHERE guid = '%u'", player->GetGUIDLow())) 
                        {//if the name is in the lottery table
                            ChatHandler(player->GetSession()).PSendSysMessage("|cffff0000[Warning]: You have already bought your ticket!");
                            player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                        }
                        else //buy the ticket
                        {
                            player->ModifyMoney(-20000); //remove 2 golds to the player
                //put the char name on the db
                            WorldDatabase.PExecute("INSERT INTO lottery_players (guid, name) VALUES ('%u', '%s')", player->GetGUIDLow(), player->GetName());
                            ChatHandler(player->GetSession()).PSendSysMessage("You bought a ticket!, i hope you will be the next winner!");
                            player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                        }
                    }
                    else //if player don't have enought money
                    {
                        ChatHandler(player->GetSession()).PSendSysMessage("|cffff0000[Warning]: You don't have enought golds to buy the lottery ticket!");
                        player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                    }
                } 
                break;

                case END: // should be automated
                { 
                    WorldDatabase.PExecute("DELETE FROM lottery_winner");
                    if (QueryResult winner = WorldDatabase.PQuery("SELECT guid FROM lottery_players ORDER BY RAND() LIMIT 1"))
                    {
                        Field* field = winner->Fetch();
                        if (CharacterNameData const* data = sWorld->GetCharacterNameData(ObjectGuid(HIGHGUID_PLAYER, field[0].GetUInt32())))
                            WorldDatabase.PExecute("INSERT lottery_winner (guid, name) VALUES ('%u', '%s')", player->GetGUIDLow(), data->m_name.c_str());
                        CharacterDatabase.PExecute("INSERT INTO mail (id, stationery, sender, receiver, subject, body, expire_time, deliver_time, money, checked) VALUES ('0', '61', '1','%u', '[Lottery System]', 'Congratulations you are the new lottery winner!, you are very lucky!', '1448470268', '1441051698', '30000000', '16')",  player->GetGUIDLow()); 
                    }
                    sWorld->SendServerMessage(SERVER_MSG_STRING, "|cffff0000[Event Message]: The lottery has been conclused! Congratulations to winner!");
                    WorldDatabase.PExecute("DELETE FROM lottery_players");
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;
        }
        return true;
      }
};				

void AddSC_npc_lottery()
{        
	new npc_lottery();
}		
