#include "ScriptPCH.h"
#include "Pet.h"
 
enum Pets
{
  HYENA = 4127,
  RAVAGER = 17527,
  BAT = 1554,
  BOAR = 345,
  SPIDER = 2349,
  WOLF = 3823,
  RAPTOR = 3637,
  BEAR = 17348,
  CRAB = 2233,
  ADDER = 5048,
};

enum Action
{
  BUY,
  STABLE,
  PET,
  MENU,
};

class npc_beastmaster : public CreatureScript 
{
    public:
        npc_beastmaster() : CreatureScript("npc_beastmaster") { }
    
        void CreatePet(Player * player, Creature * creature, uint32 entry) 
        { 
            if(player->GetPet()) 
            {
                ChatHandler(player->GetSession()).PSendSysMessage("|cffff0000[Warning]: To add a new pet you should first drop your current Pet!");
                player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                return;
            }
            Creature *creatureTarget = creature->SummonCreature(entry, player->GetPositionX(), player->GetPositionY()+2, player->GetPositionZ(), player->GetOrientation(), TEMPSUMMON_CORPSE_TIMED_DESPAWN, 500);
            if(!creatureTarget)
                return ;
            Pet* pet = player->CreateTamedPetFrom(creatureTarget, 0);
            if(!pet)
                return ;
            creatureTarget->setDeathState(JUST_DIED);
            creatureTarget->RemoveCorpse();
            creatureTarget->SetHealth(0); // just for nice GM-mode view
            pet->SetPower(POWER_HAPPINESS, 1048000);
            pet->SetUInt64Value(UNIT_FIELD_CREATEDBY, player->GetGUID());
            pet->SetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE, player->getFaction());
            pet->SetUInt32Value(UNIT_FIELD_LEVEL, player->getLevel() - 1);
            pet->GetMap()->AddToMap(pet->ToCreature());
            pet->SetUInt32Value(UNIT_FIELD_LEVEL, player->getLevel());
            pet->GetCharmInfo()->SetPetNumber(sObjectMgr->GeneratePetNumber(), true);
            if(!pet->InitStatsForLevel(player->getLevel()))
                TC_LOG_ERROR("misc", "Fail: no init stats for entry %u", entry);
            pet->UpdateAllStats();
            player->SetMinion(pet, true);
            pet->SavePetToDB(PET_SAVE_AS_CURRENT);
            pet->InitTalentForLevel();
            player->PetSpellInitialize();
            player->PlayerTalkClass->SendCloseGossip();
            ChatHandler(player->GetSession()).PSendSysMessage("Pet successfully added!");
        }

        bool OnGossipHello(Player * player, Creature * creature)
        {
            player->ADD_GOSSIP_ITEM(9, "[Pets] ->", GOSSIP_SENDER_MAIN, MENU);
            player->PlayerTalkClass->SendGossipMenu(907, creature->GetGUID());
            return true;
        }

        bool OnGossipSelect(Player * player, Creature * creature, uint32 /*sender*/, uint32 Action)
        {
            switch(Action)
            {
                case MENU:
                {
                    if(player->getClass() == CLASS_HUNTER) 
                    {
                        player->PlayerTalkClass->ClearMenus();
                        player->ADD_GOSSIP_ITEM(4, "Pets", GOSSIP_SENDER_MAIN, PET);
                        player->ADD_GOSSIP_ITEM(1, "Buy food", GOSSIP_SENDER_MAIN, BUY);
                        player->ADD_GOSSIP_ITEM(0, "Stable", GOSSIP_SENDER_MAIN, STABLE);
                        player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                    } 
                    else
                    {
                        player->GetSession()->SendNotification("You are not an hunter!");
                        player->PlayerTalkClass->SendCloseGossip();
                    }
                    return true;
                }
                break;

                case PET:
                {
                    player->PlayerTalkClass->ClearMenus();
                    player->ADD_GOSSIP_ITEM(4, "Hyena", GOSSIP_SENDER_MAIN, HYENA);
                    player->ADD_GOSSIP_ITEM(4, "Ravager", GOSSIP_SENDER_MAIN, RAVAGER);
                    player->ADD_GOSSIP_ITEM(4, "Bat", GOSSIP_SENDER_MAIN, BAT);
                    player->ADD_GOSSIP_ITEM(4, "Boar", GOSSIP_SENDER_MAIN, BOAR);
                    player->ADD_GOSSIP_ITEM(4, "Spider", GOSSIP_SENDER_MAIN, SPIDER);
                    player->ADD_GOSSIP_ITEM(4, "Wolf", GOSSIP_SENDER_MAIN, WOLF);
                    player->ADD_GOSSIP_ITEM(4, "Raptor", GOSSIP_SENDER_MAIN, RAPTOR);
                    player->ADD_GOSSIP_ITEM(4, "Bear", GOSSIP_SENDER_MAIN, BEAR);
                    player->ADD_GOSSIP_ITEM(4, "Crab", GOSSIP_SENDER_MAIN, CRAB);
                    player->ADD_GOSSIP_ITEM(4, "Adder", GOSSIP_SENDER_MAIN, ADDER);
                    player->ADD_GOSSIP_ITEM(0, "Back", GOSSIP_SENDER_MAIN, MENU);
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;

                case HYENA:
                {
                    CreatePet(player, creature, Action); 
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;

                case RAVAGER:
                { 
                    CreatePet(player, creature, Action);
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;

                case BAT:
                {
                    CreatePet(player, creature, Action);
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;

                case BOAR:
                {
                    CreatePet(player, creature, Action);
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;

                case SPIDER: 
                {
                    CreatePet(player, creature, Action);
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;

                case WOLF:
                {
                    CreatePet(player, creature, Action);
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;

                case RAPTOR:
                {
                    CreatePet(player, creature, Action);
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;

                case BEAR:
                {
                    CreatePet(player, creature, Action);
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;

                case CRAB:
                {
                    CreatePet(player, creature, Action);
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;
 
                case ADDER:
                {
                   CreatePet(player, creature, Action);
                   player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;

                case BUY:
                    player->GetSession()->SendListInventory(creature->GetGUID());
                break;

                case STABLE:
                    player->GetSession()->SendStablePet(creature->GetGUID());
                break;
            } 
            return true;
        }
};
 
void AddSC_npc_beastmaster()
{
    new npc_beastmaster();
}
