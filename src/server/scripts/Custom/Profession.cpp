#include "ScriptPCH.h"
#include "Language.h"

enum Professions
{
  MENU,
  ALCHEMY,
  LEARN_ALCHEMY,
  UNLEARN_ALCHEMY,
  BLACKSMITHING,
  LEARN_BLACKSMITHING,
  UNLEARN_BLACKSMITHING,
  LEATHERWORKING,
  LEARN_LEATHERWORKING,
  UNLEARN_LEATHERWORKING,
  TAILORING,
  LEARN_TAILORING,
  UNLEARN_TAILORING,
  ENGINEERING,
  LEARN_ENGINEERING,
  UNLEARN_ENGINEERING,
  ENCHANTING,
  LEARN_ENCHANTING,
  UNLEARN_ENCHANTING,
  JEWELCRAFTING,
  LEARN_JEWELCRAFTING,
  UNLEARN_JEWELCRAFTING,
  INSCRIPTION,
  LEARN_INSCRIPTION,
  UNLEARN_INSCRIPTION,
  HERBALISM,
  LEARN_HERBALISM,
  UNLEARN_HERBALISM,
  SKINNING,
  LEARN_SKINNING,
  UNLEARN_SKINNING,
  MINING,
  LEARN_MINING,
  UNLEARN_MINING,
};


class professions_npc : public CreatureScript
{
  public:
          professions_npc () : CreatureScript("professions_npc") { }
                   
          void CreatureWhisperBasedOnBool(const char *text, Creature * creature, Player * player, bool value)
          {
            if (value)
              creature->TextEmote(text, player);
           }
     
           uint32 PlayerMaxLevel() const
           {
             return sWorld->getIntConfig(CONFIG_MAX_PLAYER_LEVEL);
           }
     
           bool PlayerHasItemOrSpell(const Player * player, uint32 itemId, uint32 spellId) const
           {
             return player->HasItemCount(itemId, 1, true) || player->HasSpell(spellId);
           }

           bool OnGossipHello(Player * player, Creature * creature)
           { 
             player->ADD_GOSSIP_ITEM(9, "[Professions] ->", GOSSIP_SENDER_MAIN, MENU);
             player->PlayerTalkClass->SendGossipMenu(907, creature->GetGUID());
             return true;
           }
                   
           bool PlayerAlreadyHasTwoProfessions(const Player * player) const
           {
              uint32 skillCount = 0;
     
              if (player->HasSkill(SKILL_MINING))
                       skillCount++;
              if (player->HasSkill(SKILL_SKINNING))
                       skillCount++;
              if (player->HasSkill(SKILL_HERBALISM))
                       skillCount++;
     
              if (skillCount >= 2)
                       return true;
     
              for (uint32 i = 1; i < sSkillLineStore.GetNumRows(); ++i)
              {
                SkillLineEntry const *SkillInfo = sSkillLineStore.LookupEntry(i);
                
                if (!SkillInfo)
                       continue;
     
                if (SkillInfo->categoryId == SKILL_CATEGORY_SECONDARY)
                       continue;
     
                if ((SkillInfo->categoryId != SKILL_CATEGORY_PROFESSION) || !SkillInfo->canLink)
                       continue;
     
                const uint32 skillID = SkillInfo->id;
                      if (player->HasSkill(skillID))
                                 skillCount++;
     
                      if (skillCount >= 2)
                                 return true;
              }
              return false;
           }
     
           bool LearnAllRecipesInProfession(Player * player, SkillType skill)
           {
             ChatHandler handler(player->GetSession());
             char* skill_name;
             SkillLineEntry const *SkillInfo = sSkillLineStore.LookupEntry(skill);
             skill_name = SkillInfo->name[handler.GetSessionDbcLocale()];
     
             if (!SkillInfo)
             {
               TC_LOG_ERROR("server.loading", "Profession NPC: received non-valid skill ID(LearnAllRecipesInProfession)");
             }      
     
             LearnSkillRecipesHelper(player, SkillInfo->id);
     
             player->SetSkill(SkillInfo->id, player->GetSkillStep(SkillInfo->id), 450, 450);
             handler.PSendSysMessage(LANG_COMMAND_LEARN_ALL_RECIPES, skill_name);      
             return true;
           }
           
           void LearnSkillRecipesHelper(Player * player, uint32 skill_id)
           {
             uint32 classmask = player->getClassMask();
     
             for (uint32 j = 0; j < sSkillLineAbilityStore.GetNumRows(); ++j)
             {
               SkillLineAbilityEntry const *skillLine = sSkillLineAbilityStore.LookupEntry(j);
               if (!skillLine)
                      continue;
     
               // wrong skill
               if (skillLine->skillId != skill_id)
                      continue;
     
               // not high rank
               if (skillLine->forward_spellid)
                      continue;
     
               // skip racial skills
               if (skillLine->racemask != 0)
                      continue;
     
               // skip wrong class skills
               if (skillLine->classmask && (skillLine->classmask & classmask) == 0)
                      continue;
     
               SpellInfo const * spellInfo = sSpellMgr->GetSpellInfo(skillLine->spellId);
               if (!spellInfo || !SpellMgr::IsSpellValid(spellInfo, player, false))
                      continue;
                                   
               player->LearnSpell(skillLine->spellId, false);
             }
           }
     
           bool IsSecondarySkill(SkillType skill) const
           {
             return skill == SKILL_COOKING || skill == SKILL_FIRST_AID;
           }
     
           void CompleteLearnProfession(Player * player, Creature * /*creature*/, SkillType skill)
           {
             if (PlayerAlreadyHasTwoProfessions(player) && !IsSecondarySkill(skill))
             {
               player->GetSession()->SendNotification("You already know two professions!");             
             }
             else
             {
               if (!LearnAllRecipesInProfession(player, skill))
                      player->GetSession()->SendNotification("Internal error occured!"); 
             }
           }

           bool ForgotSkill(Player * player, SkillType skill)
           {
             if (!player->HasSkill(skill))
             {
               player->GetSession()->SendNotification("You don't have that skill!");
                 return false;
             }
             SkillLineEntry const* SkillInfo = sSkillLineStore.LookupEntry(skill);
             if (!SkillInfo)
             {
               //TC_LOG_ERROR("entitles.player.skills", TXT_BAD_SKILL);
                 return false;
             }
             player->SetSkill(SkillInfo->id, player->GetSkillStep(SkillInfo->id), 0, 0);
             return true;
           }
           
           bool OnGossipSelect(Player * player, Creature * creature, uint32 /*Sender*/, uint32 Action)
           {
             switch(Action)
             {
               case MENU:
               {
                 player->PlayerTalkClass->ClearMenus();
                 player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\trade_alchemy:30|t Alchemy", GOSSIP_SENDER_MAIN, ALCHEMY);
                 player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\INV_Ingot_05:30|t Blacksmithing", GOSSIP_SENDER_MAIN, BLACKSMITHING);
                 player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\INV_Misc_LeatherScrap_02:30|t Leatherworking", GOSSIP_SENDER_MAIN, LEATHERWORKING);
                 player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\INV_Fabric_Felcloth_Ebon:30|t Tailoring", GOSSIP_SENDER_MAIN, TAILORING);
                 player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\inv_misc_wrench_01:30|t Engineering", GOSSIP_SENDER_MAIN, ENGINEERING);
                 player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\trade_engraving:30|t Enchanting", GOSSIP_SENDER_MAIN, ENCHANTING);
                 player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\inv_misc_gem_01:30|t Jewelcrafting", GOSSIP_SENDER_MAIN, JEWELCRAFTING);
                 player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\INV_Scroll_08:30|t Inscription", GOSSIP_SENDER_MAIN, INSCRIPTION);
                 player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\INV_Misc_Herb_07:30|t Herbalism", GOSSIP_SENDER_MAIN, HERBALISM);
                 player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\inv_misc_pelt_wolf_01:30|t Skinning", GOSSIP_SENDER_MAIN, SKINNING);
                 player->ADD_GOSSIP_ITEM(0, "|TInterface\\icons\\trade_mining:30|t Mining", GOSSIP_SENDER_MAIN, MINING);
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
               break;

               case ALCHEMY: //ALCHEMY MENU
               {
                 player->PlayerTalkClass->ClearMenus();
                 player->ADD_GOSSIP_ITEM_EXTENDED(3, "Learn Alchemy", GOSSIP_SENDER_MAIN, LEARN_ALCHEMY,"Are you sure to learn Alchemy?", 0,false);
                 player->ADD_GOSSIP_ITEM_EXTENDED(3, "Unlearn Alchemy", GOSSIP_SENDER_MAIN, UNLEARN_ALCHEMY, "Are you sure to unlearn Alchemy?", 0, false);
                 player->ADD_GOSSIP_ITEM(0, "Back", GOSSIP_SENDER_MAIN, MENU);
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
               break;
               
               case LEARN_ALCHEMY: //LEARN ALCHEMY
               {
                 if(player->HasSkill(SKILL_ALCHEMY))
                 {
                   player->GetSession()->SendNotification("You already have the Alchemy skill!");
                   player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                 }
                 else
                 {
                   CompleteLearnProfession(player, creature, SKILL_ALCHEMY);
                   player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                 }
               }
               break;

               case UNLEARN_ALCHEMY: //REMOVE ALCHEMY
               {
                 ForgotSkill(player, SKILL_ALCHEMY);
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
               break;
            
               case BLACKSMITHING: //BLACKSMITHING MENU
               {
                 player->PlayerTalkClass->ClearMenus();
                 player->ADD_GOSSIP_ITEM_EXTENDED(3, "Learn Blacksmithing", GOSSIP_SENDER_MAIN, LEARN_BLACKSMITHING,"Are you sure to learn Blacksmithing?", 0,false);
                 player->ADD_GOSSIP_ITEM_EXTENDED(3, "Unlearn Blacksmithing", GOSSIP_SENDER_MAIN, UNLEARN_BLACKSMITHING, "Are you sure to unlearn Blacksmithing?", 0, false); 
                 player->ADD_GOSSIP_ITEM(0, "Back", GOSSIP_SENDER_MAIN, MENU);
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
               break;
 
               case LEARN_BLACKSMITHING: //LEARN BLACKSMITHING
               {
                 if(player->HasSkill(SKILL_BLACKSMITHING))
                 {
                   player->GetSession()->SendNotification("You already have the Blacksmithing skill!");
                   player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                 }
                 else
                 {
                   CompleteLearnProfession(player, creature, SKILL_BLACKSMITHING);
                   player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                 }
               }
               break;

               case UNLEARN_BLACKSMITHING: //REMOVE BLACKSMITHING
               {
                 ForgotSkill(player, SKILL_BLACKSMITHING);
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
               break;
                             
               case LEATHERWORKING: //LEATHERWORKING MENU
               {
                 player->PlayerTalkClass->ClearMenus();
                 player->ADD_GOSSIP_ITEM_EXTENDED(3, "Learn Leatherworking", GOSSIP_SENDER_MAIN, LEARN_LEATHERWORKING,"Are you sure to learn Leatherworking?", 0,false);
                 player->ADD_GOSSIP_ITEM_EXTENDED(3, "Unlearn Leatherworking", GOSSIP_SENDER_MAIN, UNLEARN_LEATHERWORKING, "Are you sure to unlearn Leatherworking?", 0, false);
                 player->ADD_GOSSIP_ITEM(0, "Back", GOSSIP_SENDER_MAIN, MENU);
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
               break;
 
               case LEARN_LEATHERWORKING: //LEARN LEATHERWORKING
               {
                 if(player->HasSkill(SKILL_LEATHERWORKING))
                 {
                   player->GetSession()->SendNotification("You already have the Leatherworking skill!");
                   player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                 }
                 else
                 {
                   CompleteLearnProfession(player, creature, SKILL_LEATHERWORKING);
                   player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                 }
              }
              break;

              case UNLEARN_LEATHERWORKING: //REMOVE LEATHERWORKING
              {
                ForgotSkill(player, SKILL_LEATHERWORKING);
                player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
              }
              break;
                                    
              case TAILORING: //TAILORING MENU
              {
                player->PlayerTalkClass->ClearMenus();
                player->ADD_GOSSIP_ITEM_EXTENDED(3, "Learn Tailoring", GOSSIP_SENDER_MAIN, LEARN_TAILORING,"Are you sure to learn Tailoring?", 0,false);
                player->ADD_GOSSIP_ITEM_EXTENDED(3, "Unlearn Tailoring", GOSSIP_SENDER_MAIN, UNLEARN_TAILORING, "Are you sure to unlearn Tailoring?", 0, false);
                player->ADD_GOSSIP_ITEM(0, "Back", GOSSIP_SENDER_MAIN, MENU);
                player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
              }
              break;

             case LEARN_TAILORING: //LEARN TAILORING
             {
               if(player->HasSkill(SKILL_TAILORING))
               {
                 player->GetSession()->SendNotification("You already have the Tailoring skill!");
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
               else
               {
                 CompleteLearnProfession(player, creature, SKILL_TAILORING);
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
             }
             break;

             case UNLEARN_TAILORING: //REMOVE TAILORING
             {
               ForgotSkill(player, SKILL_TAILORING);
               player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
             }
             break;
                             
             case ENGINEERING: //ENGINEERING MENU
             {
               player->PlayerTalkClass->ClearMenus();
               player->ADD_GOSSIP_ITEM_EXTENDED(3, "Learn Engineering", GOSSIP_SENDER_MAIN, LEARN_ENGINEERING,"Are you sure to learn Engineering?", 0, false);
               player->ADD_GOSSIP_ITEM_EXTENDED(3, "Unlearn Engineering", GOSSIP_SENDER_MAIN, UNLEARN_ENGINEERING, "Are you sure to unlearn Engineering?", 0, false);
               player->ADD_GOSSIP_ITEM(0, "Back", GOSSIP_SENDER_MAIN, MENU);
               player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
             } 
             break;

             case LEARN_ENGINEERING: //LEARN ENGINEERING
             {
               if(player->HasSkill(SKILL_ENGINEERING))
               {
                 player->GetSession()->SendNotification("You already have the Engineering skill!");
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
               else
               {            
                 CompleteLearnProfession(player, creature, SKILL_ENGINEERING);
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
             }
             break;

             case UNLEARN_ENGINEERING: //REMOVE ENGINEERING
             {
               ForgotSkill(player, SKILL_ENGINEERING);
               player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
             }
             break;
                             
             case ENCHANTING: //ENCHANTING MENU
             {
               player->PlayerTalkClass->ClearMenus();
               player->ADD_GOSSIP_ITEM_EXTENDED(3, "Learn Enchanting", GOSSIP_SENDER_MAIN, LEARN_ENCHANTING,"Are you sure to learn Enchanting?", 0, false);
               player->ADD_GOSSIP_ITEM_EXTENDED(3, "Unlearn Enchanting", GOSSIP_SENDER_MAIN, UNLEARN_ENCHANTING, "Are you sure to unlearn Enchanting?", 0, false);
               player->ADD_GOSSIP_ITEM(0, "Back", GOSSIP_SENDER_MAIN, MENU);
               player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
             }
             break;
 
             case LEARN_ENCHANTING: //LEARN ENCHANTING
             {
               if(player->HasSkill(SKILL_ENCHANTING))
               {
                 player->GetSession()->SendNotification("You already have the Enchanting skill!");
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
               else
               {
                 CompleteLearnProfession(player, creature, SKILL_ENCHANTING);
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
             }
             break;

             case UNLEARN_ENCHANTING: //UNLEARN ENCHANTING
             {
               ForgotSkill(player, SKILL_ENCHANTING);
               player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
             }
                             
             case JEWELCRAFTING: //JEWELCRAFTING MENU
             {
               player->PlayerTalkClass->ClearMenus();
               player->ADD_GOSSIP_ITEM_EXTENDED(3, "Learn Jewelcrafting", GOSSIP_SENDER_MAIN, LEARN_JEWELCRAFTING,"Are you sure to learn Jewelcrafting?", 0, false);
               player->ADD_GOSSIP_ITEM_EXTENDED(3, "Unlearn Jewelcrafting", GOSSIP_SENDER_MAIN, UNLEARN_JEWELCRAFTING, "Are you sure to unlearn Jewelcrafting?", 0, false);
               player->ADD_GOSSIP_ITEM(0, "Back", GOSSIP_SENDER_MAIN, MENU);
               player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
             }
             break;

             case LEARN_JEWELCRAFTING: //LEARN JEWELCRAFTING
             {
               if(player->HasSkill(SKILL_JEWELCRAFTING))
               {
                 player->GetSession()->SendNotification("You already have the Jewelcrafting skill!");
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
               else
               {
                 CompleteLearnProfession(player, creature, SKILL_JEWELCRAFTING);
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
             }
             break;

             case UNLEARN_JEWELCRAFTING: //UNLEARN JEWELCRAFTING
             {
               ForgotSkill(player, SKILL_JEWELCRAFTING);
               player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
             }
             break;
                            
             case INSCRIPTION: //INSCRIPTION MENU
             {
               player->PlayerTalkClass->ClearMenus();
               player->ADD_GOSSIP_ITEM_EXTENDED(3, "Learn Inscription", GOSSIP_SENDER_MAIN, LEARN_INSCRIPTION,"Are you sure to learn Inscription?", 0, false);
               player->ADD_GOSSIP_ITEM_EXTENDED(3, "Unlearn Inscription", GOSSIP_SENDER_MAIN, UNLEARN_INSCRIPTION, "Are you sure to unlearn Inscription?", 0, false);
               player->ADD_GOSSIP_ITEM(0, "Back", GOSSIP_SENDER_MAIN, MENU);
               player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
             }
             break;

             case LEARN_INSCRIPTION: //LEARN INSCRIPTION
             {
               if(player->HasSkill(SKILL_INSCRIPTION))
               {
                 player->GetSession()->SendNotification("You already have the Inscription skill!");
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
               else
               {
                 CompleteLearnProfession(player, creature, SKILL_INSCRIPTION);
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }                                                                                    
             }
             break;

             case UNLEARN_INSCRIPTION: //REMOVE INSCRIPTION
             {
               ForgotSkill(player, SKILL_INSCRIPTION);
               player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
             }
             break;
                     
             case HERBALISM: //HERBALISM
             {
               player->PlayerTalkClass->ClearMenus();
               player->ADD_GOSSIP_ITEM_EXTENDED(3, "Learn Herbalism", GOSSIP_SENDER_MAIN, LEARN_HERBALISM,"Are you sure to learn Herbalism?", 0, false);
               player->ADD_GOSSIP_ITEM_EXTENDED(3, "Unlearn Herbalism", GOSSIP_SENDER_MAIN, UNLEARN_HERBALISM, "Are you sure to unlearn Herbalism?", 0, false);
               player->ADD_GOSSIP_ITEM(0, "Back", GOSSIP_SENDER_MAIN, MENU);
               player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
             }
             break;

             case LEARN_HERBALISM: //LEARN HERBALISM
             {
               if(player->HasSkill(SKILL_HERBALISM))
               {
                 player->GetSession()->SendNotification("You already have the Herbalism skill!");
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
               else
               {                                        
                 CompleteLearnProfession(player, creature, SKILL_HERBALISM);
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }
             }
             break;

             case UNLEARN_HERBALISM: //REMOVE HERBALISM
             {
               ForgotSkill(player, SKILL_HERBALISM);
               player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
             }
             break;                     
                   
             case SKINNING: //SKINNING MENU
             {
               player->PlayerTalkClass->ClearMenus();
               player->ADD_GOSSIP_ITEM_EXTENDED(3, "Learn Skinning", GOSSIP_SENDER_MAIN, LEARN_SKINNING,"Are you sure to learn Skinning?", 0, false);
               player->ADD_GOSSIP_ITEM_EXTENDED(3, "Unlearn Skinning", GOSSIP_SENDER_MAIN, UNLEARN_SKINNING, "Are you sure to unlearn Skinning?", 0, false);
               player->ADD_GOSSIP_ITEM(0, "Back", GOSSIP_SENDER_MAIN, MENU);
               player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
             }
             break;

             case LEARN_SKINNING: //LEARN SKINNING
             {
               if(player->HasSkill(SKILL_SKINNING))
               {
                 player->GetSession()->SendNotification("You already have the Skinning skill!");
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
               }                                                    
               else
               {
                 CompleteLearnProfession(player, creature, SKILL_SKINNING);
                 player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID()); 
               }
             }
             break;

            case UNLEARN_SKINNING: //REMOVE SKINNING
            {
              ForgotSkill(player, SKILL_SKINNING);
              player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
            }
            break;
                                                                                    
            case MINING: //MINING MENU
            {
              player->PlayerTalkClass->ClearMenus();
              player->ADD_GOSSIP_ITEM_EXTENDED(3, "Learn Mining", GOSSIP_SENDER_MAIN, LEARN_MINING,"Are you sure to learn Mining?", 0, false);
              player->ADD_GOSSIP_ITEM_EXTENDED(3, "Unlearn Mining", GOSSIP_SENDER_MAIN, UNLEARN_MINING, "Are you sure to unlearn Mining?", 0, false);
              player->ADD_GOSSIP_ITEM(0, "Back", GOSSIP_SENDER_MAIN, MENU);
              player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
            }
            break;

            case LEARN_MINING: //LEARN MINING
            {
              if(player->HasSkill(SKILL_MINING))
              {
                player->GetSession()->SendNotification("You already have the Mining skill!");
                player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
              }
              else
              {                                                                                           
                CompleteLearnProfession(player, creature, SKILL_MINING);  
                player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
              }
            }
            break;
          
            case UNLEARN_MINING: //REMOVE MINING
            {
              ForgotSkill(player, SKILL_MINING);
              player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
            }
               
          }
          return true;
        }
    };
     
    void AddSC_professions_npc()
    {
        new professions_npc();
    }
