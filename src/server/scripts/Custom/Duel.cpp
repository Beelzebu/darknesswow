#include "ScriptPCH.h"

class duel_reset : public PlayerScript
{
    public:
        duel_reset() : PlayerScript("duel_reset") { }

        void OnDuelStart(Player * player1, Player * player2)
        {
            player1->SetHealth(player1->GetMaxHealth());
            player2->SetHealth(player2->GetMaxHealth());

            switch (player1->getPowerType())
            {
                case POWER_MANA:
                    player1->SetPower(POWER_MANA, player1->GetMaxPower(POWER_MANA));
                    break;
                case POWER_RAGE:
                    player1->SetPower(POWER_RAGE, 0);
                    break;
                case POWER_ENERGY:
                    player1->SetPower(POWER_ENERGY, player1->GetMaxPower(POWER_ENERGY));
                    break;
                case POWER_RUNIC_POWER:
                    player1->SetPower(POWER_RUNIC_POWER, 0);
                    break;
                default:
                    break;
            }

            switch (player2->getPowerType())
            {
                case POWER_MANA:
                    player2->SetPower(POWER_MANA, player2->GetMaxPower(POWER_MANA));
                    break;
                case POWER_RAGE:
                    player2->SetPower(POWER_RAGE, 0);
                    break;
                case POWER_ENERGY:
                    player2->SetPower(POWER_ENERGY, player2->GetMaxPower(POWER_ENERGY));
                    break;
                case POWER_RUNIC_POWER:
                    player2->SetPower(POWER_RUNIC_POWER, 0);
                    break;
                default:
                    break;    
            }
        }

        void OnDuelEnd(Player * winner, Player * loser, DuelCompleteType type)
        {
            if (type == DUEL_WON)
            {
                winner->RemoveArenaSpellCooldowns();
                loser->RemoveArenaSpellCooldowns();
                winner->SetHealth(winner->GetMaxHealth());
                loser->SetHealth(loser->GetMaxHealth());

                switch (winner->getPowerType())
                {
                    case POWER_MANA:
                        winner->SetPower(POWER_MANA, winner->GetMaxPower(POWER_MANA));
                        break;
                    case POWER_RAGE:
                        winner->SetPower(POWER_RAGE, 0);
                        break;
                    case POWER_ENERGY:
                        winner->SetPower(POWER_ENERGY, winner->GetMaxPower(POWER_ENERGY));
                        break;
                    case POWER_RUNIC_POWER:
                        winner->SetPower(POWER_RUNIC_POWER, 0);
                        break;
                    default:
                        break;
                }

                switch (loser->getPowerType())
                {
                    case POWER_MANA:
                        loser->SetPower(POWER_MANA, loser->GetMaxPower(POWER_MANA));
                        break;
                    case POWER_RAGE:
                        loser->SetPower(POWER_RAGE, 0);
                        break;
                    case POWER_ENERGY:
                        loser->SetPower(POWER_ENERGY, loser->GetMaxPower(POWER_ENERGY));
                        break;
                    case POWER_RUNIC_POWER:
                        loser->SetPower(POWER_RUNIC_POWER, 0);
                        break;
                    default:
                        break;    
                }
            }
        }
};

void AddSC_duel_reset()
{
    new duel_reset();
}
