#include "ScriptPCH.h"

class npc_donation : public CreatureScript
{
    public:
        npc_donation() : CreatureScript("npc_donation") { }

	bool OnGossipHello(Player * player, Creature * creature)
        {
            player->ADD_GOSSIP_ITEM(7, "How can i donate?", GOSSIP_SENDER_MAIN, 1);
            player->ADD_GOSSIP_ITEM(7, "How can i get VIP account?", GOSSIP_SENDER_MAIN, 2);
            player->ADD_GOSSIP_ITEM(7, "What are VIP vendors?", GOSSIP_SENDER_MAIN, 3);
            player->ADD_GOSSIP_ITEM(7, "How much i should pay for a VIP account?", GOSSIP_SENDER_MAIN, 4);
            player->ADD_GOSSIP_ITEM(7, "What can i recive with VIP account?", GOSSIP_SENDER_MAIN, 5);
	    player->ADD_GOSSIP_ITEM(7, "Which commands are available on VIP account?", GOSSIP_SENDER_MAIN, 6);
            player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
            return true;
	}			
	 
        bool OnGossipSelect(Player * player, Creature * creature, uint32 /*uiSender*/, uint32 Action)
        { 
            switch(Action)
            {        
                case 1:
                {      
                    ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[How can i donate?]:|r Hello Dear Player, if you wish to donate please go on http://www.darknesswow.com/donate and login with your account, then simply select a method to donate and read cerfully the steps to do.");
	            player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;

                case 2:
                {
                    ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[How can i get a VIP account?]:|r Hello Dear Player, if you want get a VIP account you need to go on http://www.darknesswow.com/donate and login with your account, then select buy ''VIP Account''. ");		
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;

                case 3:
                {
                    ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[What are VIP vendors?]:|r Hello Dear Player, VIP vendors sell mounts, pets, armors, weapons all for free, you can find VIP vendors only in the VIP mall.");							
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;	
		
                case 4:
                {
                    ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[How much i should pay for a VIP account?]:|r Hello Dear Player, if you want to get a VIP account you must go on www.darknesswow.com/donate and then select buy ''VIP Account'', the price is 20$.");		
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;
           
                case 5:
                {
                    ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[What can i recive with VIP account?]:|r With a VIP account you will recieve mounts, pets, armors, weapons and grants you VIP commands on your account. Also grant you access to specials VIP Areas.");
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;
           
                case 6:
                {
                    ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Which commands are available on VIP account?]:|r Hello Dear Player, a few commands available on VIP account are: .tele vipmall takes you to vip mall; .tele vipisland teleports you to VIP exclusive queue place; .appear takes you to the character whose name you entered; .commands show you a list of all commands.");				
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
                break;
            }
            return true;
        }
};				

void AddSC_npc_donation()
{        
     new npc_donation();
}
