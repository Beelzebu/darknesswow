/*
#include "ScriptPCH.h"

enum Actions
{
  START,
  BACK,
  INFO,
};

class npc_event : public CreatureScript
{
  public:
          npc_event() : CreatureScript("npc_event") { }

          bool OnGossipHello(Player * player, Creature * creature)
          {
            if(player->GetQuestStatus(20000) == QUEST_STATUS_COMPLETE)
            { 
              player->ADD_GOSSIP_ITEM_EXTENDED(0, "Teleport me back!", GOSSIP_SENDER_MAIN, BACK, "Are you sure to be teleported back?", 0, false);
              player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
            }
            else
            {
              player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "[INFO] How the event works?", GOSSIP_SENDER_MAIN, INFO); 
              player->ADD_GOSSIP_ITEM_EXTENDED(4, "Start the event!", GOSSIP_SENDER_MAIN, START, "Are you sure to start the event?", 0, false);
              player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
            }
            return true;
          }			
         
          bool OnGossipSelect(Player * player, Creature * creature, uint32 /*uiSender, uint32 Action)
          { 
            switch(Action)
            {
              case START:
              {
                if(player->IsActiveQuest(20000)) // check if player has the quest
                { 
                  if(!player->GetGroup()) // if player isn't in group
                  {
                    player->TeleportTo(571, 5820.903809, 423.841461, 658.783875, 5.012739);
                    player->CastSpell(player,24222, true);
                    player->RemoveSpell(54197);
                    player->PlayerTalkClass->SendCloseGossip();
                  }
                  else // if player is in group
                  {
                    player->GetSession()->SendNotification("To start the event you need to leave the group!");
                    player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                  }
                }
                else // if player doesn't have the quest
                {
                  player->GetSession()->SendNotification("To start the event you need to have the quest!");
                  player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
                }
              }
              break;

              case BACK:
              {
                ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Stairs Event]: Congratulation %s, you have completed the Stairs Event. Enjoy your rewards!", player->GetName());
                player->LearnSpell(54197, true);
                player->TeleportTo(571, 5820.903809, 423.841461, 658.783875, 5.012739);
                player->CastSpell(player, 24222, true);
                player->PlayerTalkClass->SendCloseGossip();
              }
              break;

              case INFO:
              {
               ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[How the event works?]:|r $Function under work$");
               player->PlayerTalkClass->SendGossipMenu(50055, creature->GetGUID());
              }
              break;
            }
	    return true;
           }		
};
*/
void AddSC_npc_event()
{
  // new npc_event();
}

