#include "ScriptPCH.h"
#include "Chat.h"
#include "SocialMgr.h"
 
#define FACTION_SPECIFIC 0
 
std::string GetNameLink(Player* player)
{
    std::string name = player->GetName();
    std::string color;
    switch(player->getClass())
    {
       	case CLASS_DEATH_KNIGHT:
       	    color = "|cffC41F3B";
	break;
	
        case CLASS_DRUID:
	    color = "|cffFF7D0A";
	break;

	case CLASS_HUNTER:
	    color = "|cffABD473";
	break;

	case CLASS_MAGE:
	    color = "|cff69CCF0";
	break;

	case CLASS_PALADIN:
	    color = "|cffF58CBA";
	break;

	case CLASS_PRIEST:
	    color = "|cffFFFFFF";
	break;

	case CLASS_ROGUE:
	    color = "|cffFFF569";
	break;

	case CLASS_SHAMAN:
	    color = "|cff0070DE";
	break;

	case CLASS_WARLOCK:
	    color = "|cff9482C9";
	break;

	case CLASS_WARRIOR:
	    color = "|cffC79C6E";
	break;
    }
    return "|Hplayer:"+name+"|h|cffffffff["+color+name+"|cffffffff]: |h|r";
}
 
class World_Chat : public CommandScript
{
    public:
        World_Chat() : CommandScript("World_Chat"){}
 
    ChatCommand * GetCommands() const
    {
        static ChatCommand WorldChatCommandTable[] =
        {
            {"chat",       rbac::RBAC_PERM_COMMAND_WORLD_CHAT, true,  &HandleWorldChatCommand,        "", NULL},
            {"c",          rbac::RBAC_PERM_COMMAND_WORLD_CHAT, true,  &HandleWorldChatCommand,        "", NULL},
            {NULL,          0,                                 false,                    NULL,        "", NULL}
        };
        return WorldChatCommandTable;
    }
 
    static bool HandleWorldChatCommand(ChatHandler * handler, const char * args)
    {
        if (!handler->GetSession()->GetPlayer()->CanSpeak())
            return false;
        std::string temp = args;
 
        if (!args || temp.find_first_not_of(' ') == std::string::npos)
            return false;
 
        std::string msg = "";
        Player * player = handler->GetSession()->GetPlayer();
 
        switch(player->GetSession()->GetSecurity())
        {
            // Player
            case SEC_PLAYER:
                msg += "|cffffffff <Player|cffffffff> ";
                msg += GetNameLink(player);
                msg += "|cffffffff";
            break;
                
            // VIP
            case SEC_MODERATOR:
                msg += "|cffffffff <|cffFFFF00V.I.P|cffffffff> ";
                msg += GetNameLink(player);
                msg += "|cffffffff";
            break;

            // MODERATOR
            /*case SEC_MODERATOR:
                msg += "|cffffffff <|cff49A835Moderator|cffffffff> ";
                msg += GetNameLink(player);
                msg += "|cffffffff";
            break;
            */

            // GAMEMASTER
            case SEC_GAMEMASTER:
                msg += "|cffffffff <|cff3B00FFGame Master|cffffffff> ";
                msg += GetNameLink(player);
                msg += "|cffffffff";
            break;

            // ADMINISTRATOR
            case SEC_ADMINISTRATOR:
                msg += "|cffffffff <|cffC41F3BAdministrator|cffffffff> ";
                msg += GetNameLink(player);
                msg += "|cffffffff";
            break;
            
            // CONSOLE
            case SEC_CONSOLE:
                msg += "|cffffffff <|cffDA70D6Server|cffffffff> ";
                msg += " |cfffaeb00";
            break;
        }
               
        msg += args;
        SessionMap sessions = sWorld->GetAllSessions();
        for (SessionMap::iterator itr = sessions.begin(); itr != sessions.end(); ++itr)
        {
            Player* plr = itr->second->GetPlayer();
            if (!plr)
            continue;

            //if (plr->GetTeam() != player->GetTeam() && FACTION_SPECIFIC)
            //continue;

            if (plr->GetSocial()->HasIgnore(player->GetGUIDLow()))
            continue;

            sWorld->SendServerMessage(SERVER_MSG_STRING, msg.c_str(), plr);
        }
        return true;
    }
};
 
void AddSC_World_Chat()
{
    new World_Chat();
}

